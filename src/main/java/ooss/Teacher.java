package ooss;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class Teacher extends Person {
    private List<Klass> klasses;
    public Teacher(int id, String name, int age) {
        super(id, name, age);
        this.klasses = new ArrayList<>();
    }

    public void assignTo(Klass klass) {
        klasses.add(klass);
    }

    public boolean belongsTo(Klass klass){
        return klasses.contains(klass);
    }

    public boolean isTeaching(Student student){

        return klasses.stream().anyMatch(student::isIn);
    }

    public void notifying(Integer classNumber, String personName) {
        System.out.printf("I am %s, teacher of Class %d. I know %s become Leader.", this.name, classNumber, personName);
    }


    public String introduce(){
        if(!klasses.isEmpty()){
           String allklassesInvolve = klasses.stream().map(a -> Integer.toString(a.getNumber())).collect(Collectors.joining(", "));
            return super.introduce() + " I am a teacher."+ " I teach Class "+allklassesInvolve+".";
        }
        return super.introduce() + " I am a teacher.";
    }



}
