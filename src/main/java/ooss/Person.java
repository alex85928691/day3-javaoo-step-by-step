package ooss;

import java.util.Objects;

public class Person {
    private int id;
    protected String name;
    private int age;

    public Person(int id, String name, int age){
        this.id = id;
        this.name = name;
        this.age = age;
    }
    public String introduce(){

        return String.format("My name is %s. I am %d years old.",name,age);
    }

    public void notifying(Integer classNumber, String personName) {

    };

    public String getName() {
        return name;
    }


    @Override
    public boolean equals(Object o) {
        Person person = (Person) o;
        return id == person.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
