package ooss;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Klass {
    private int number;
    private Student leader;

    private List<Person> persons = new ArrayList<>();

    public Klass(int number) {
        this.number = number;
        this.leader = null;
    }

    public int getNumber() {
        return number;
    }

    public void assignLeader(Student student){
        if (student.isIn(this)){
            this.leader = student;
            this.persons.forEach((person) -> person.notifying(number, student.getName()));
        }else{
            System.out.println("It is not one of us.");
        }
    }

    public boolean isLeader(Student student){
        return student == this.leader;
    }

    public void attach(Person person) {
        this.persons.add(person);
    }







    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Klass klass = (Klass) o;
        return number == klass.number;
    }

    @Override
    public int hashCode() {
        return Objects.hash(number);
    }
}
