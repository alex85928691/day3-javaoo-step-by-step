package ooss;




public class Student extends Person {
    private Klass klass;

    public Student(int id, String name, int age) {
        super(id, name, age);
    }

    public void join(Klass klass) {
        this.klass = klass;
    }

    public boolean isIn(Klass klass) {
        return this.klass == klass;
    }


    @Override
    public void notifying(Integer classNumber, String personName) {
        System.out.printf("I am %s, student of Class %d. I know %s become Leader.", this.name, classNumber, personName);
    }



    @Override
    public String introduce() {
        if(klass == null) return super.introduce() + " I am a student."+(klass != null ? " I am in class " + klass.getNumber() + "." : "");
        if(klass.isLeader(this)){
            return super.introduce() + " I am a student."+(klass != null ? " I am the leader of class " + klass.getNumber() + "." : "");
        }
        return super.introduce() + " I am a student."+(klass != null ? " I am in class " + klass.getNumber() + "." : "");
    }
}
